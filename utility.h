#ifndef UTILITY_H
#define UTILITY_H

/* ************************************************************************ */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <errno.h>

#define MAX_SIZE_OF_IP 15
#define MIN_SIZE_OF_IP 7
#define SIZE_OF_PORT 4
#define MAX_STRING 256

/* ************************************************************************ */

// Funzioni relative al controllo dell'input
void lowercase(char*);
bool strIsNumber(const char*);
bool strIsIP(const char*);
void reductoIp(char*);

//Funzione relativa alla creazione del socket
int createClientSocket(char*, int);

// Funzioni relative alla lettura e scrittura su socket
int receiveInt(int, int*);
int sendInt(int, int);
int receiveString(int, char**, int);
int sendString(int, char*, int);

// Funzione di stampa a video (STDOUT) ed errore (STDERR)
void printToMonitor(char*);
void printError(char*);

/* ************************************************************************ */

#endif