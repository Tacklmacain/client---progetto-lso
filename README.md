## Laboratorio di Sistemi Operativi
Il seguente progetto è stato sviluppato ed implementato per l'esame di “Laboratorio di Sistemi Operativi” del Corso di Laurea in Informatica all'Università di Napoli Federico II.

## Richiesta
La richiesta del progetto era di sviluppare un applicativo multithread client server per la gestione di coppie _(chiave, valore)_, l'applicativo doveva essere non centralizzato e supportare una struttura multi server e multi client.
È possibile leggere maggiori dettagli sull'implementazione nella documentazione presente nella Repository del Server.

## Obiettivi del progetto
L'obiettivo di questo progetto è creare un applicativo client-server con le seguenti caratteristiche:
- Assenza di _race conditions_ tra i pthread
- Assenza di _deadlock_ tra la comunicazione server->server e client->server
- Utilizzo corretto delle _system call_ 
- Utilizzo di I/O non bufferizzato
- Comunicazione tramite socket TCP

## Tecnologie
L'applicativo è scritto interamente in C ed è compatibile per qualsiasi sistema operativo UNIX.

## Server
Il corrispettivo applicativo Server è consultabile alla seguente repository:
https://gitlab.com/Tacklmacain/server---progetto-lso