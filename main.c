#include "utility.h"

/* ************************************************************************ */

int main(int argc, char *argv[]) 
{
    if(argc < 4 || argc > 6)
    {
        printError("Errore: numero parametri inseriti non corretto\n");
        exit(-1);
    }

    /* Salvo i valori degli elementi inseriti da terminale.
     * La disposizione è: client IPServer portaServer comando [par1] [par2] */ 
    char* serverIp = argv[1]; 
    char* serverPort = argv[2];
    char* command = argv[3];
    char* par1;
    int lengPar1;
    char* par2;
    int lengPar2;
    char* printStr = malloc(sizeof(char) * MAX_STRING);
    bzero(printStr, MAX_STRING);

    // Effettuo il lowercase sulla stringa per facilitare la strcmp
    lowercase(command);
    // store = 0 | corrupt = 1 | search = 2 | list = 3
    int commandCode;
    // Controllo se il numero di parametri inseriti corrisponde al comando selezionato
    if(strcmp(command, "list") == 0)
    {
        if(argc != 4) // "list" non prende parametri in ingresso
        {
            printError("Errore: il numero di parametri inseriti non è corretto per il comando selezionato\n");
            exit(-1);
        }
        commandCode = 3;
    }
    else if(strcmp(command, "search") == 0)
    {
        if(argc != 5) // "search" prende un solo parametro in ingresso
        {
            printError("Errore: il numero di parametri inseriti non è corretto per il comando selezionato\n");
            exit(-1);
        }
        commandCode = 2;
    }
    else if(strcmp(command, "corrupt") == 0)
    {
        if(argc != 6) // "corrupt" e prende due parametri in ingresso
        {
            printError("Errore: il numero di parametri inseriti non è corretto per il comando selezionato\n");
            exit(-1);
        }
        commandCode = 1;
    }
    else if(strcmp(command, "store") == 0)
    {
        if(argc != 6) // "store" e prende due parametri in ingresso
        {
            printError("Errore: il numero di parametri inseriti non è corretto per il comando selezionato\n");
            exit(-1);
        }
        commandCode = 0;
    }   
    else
    {
        // Se arrivo qui vuol dire che "command" non corrisponde a nessun comando valido
        printError("Errore: comando non valido\n");
        exit(-1);
    }
    
    // Effettuo il primo controllo parziale sulla stringa
    if(!strIsIP(serverIp))
    {
        printError("Errore: indirizzo IP non inserito correttamente\n");
        exit(-1);
    }

    // Formatto la stringa correttamente
    reductoIp(serverIp);

    // Se la lunghezza della porta è sbagliata, oppure non è composta da soli numero, stampo errore ed esco
    if(strlen(serverPort) != SIZE_OF_PORT || !strIsNumber(serverPort))
    {
        sprintf(printStr, "Errore: la porta deve essere formata da %d numeri\n", SIZE_OF_PORT);
        printStr[strlen(printStr)] = '\0';
        printError(printStr);
        exit(-1);
    }

    // Provo ad effettuare la connessione mediante l'apposita funzione 
    int socketDescriptor = createClientSocket(serverIp, atoi(serverPort));

    // Controllo che la connessione sia avvenuta con successo
    if(socketDescriptor < 0)
    {
        perror("Errore nella connessione al server");
        exit(-1);
    }
    else
    {
        printError("La connessine con il server è avvenuta con successo\n");
    }
    
    // Ora bisogna inviare il comando al servere e attendere risposta
    if(sendInt(socketDescriptor,commandCode) < 0)
    {
        perror("Errore");
        exit(-1);
    }
    if(commandCode == 2)
    {
        // Se il comando è search bisogna inviare solo la chiave
        par1 = argv[4];
        lengPar1 = strlen(par1) + 1;
        if(sendInt(socketDescriptor, lengPar1) < 0)
        {
            perror("Errore");
            exit(-1);
        }
        if(sendString(socketDescriptor, par1, lengPar1) < 0)
        {
            perror("Errore");
            exit(-1);
        }    
    }
    else if(commandCode == 0 || commandCode == 1)
    {
        // Se il comando è store o corrupt bisogna inviare sia chiave che valore
        par1 = argv[4];
        lengPar1 = strlen(par1) + 1;
        if(sendInt(socketDescriptor, lengPar1) < 0)
        {
            perror("Errore");
            exit(-1);
        }
        if(sendString(socketDescriptor, par1, lengPar1) < 0)
        {
            perror("Errore");
            exit(-1);
        }
        par2 = argv[5];
        lengPar2 = strlen(par2) + 1;
        if(sendInt(socketDescriptor, lengPar2) < 0)
        {
            perror("Errore");
            exit(-1);
        }
        if(sendString(socketDescriptor, par2, lengPar2) < 0)
        {
            perror("Errore");
            exit(-1);
        }
    }
    // ELSE: se è una list basta inviare il comando

    // Procedo a visualizzare la risposta del server
    int serverReturn;
    if(receiveInt(socketDescriptor, &serverReturn) < 0)
    {
        perror("Errore");
        exit(-1);
    }

    // A seconda del comando bisogna seguire un protocollo diverso
    int sizeX, sizeY;
    char *x = NULL, *y = NULL;

    switch(commandCode)
    {
        case 2: // search
            /* Nel caso della search il server inviarà 0 in caso la chiave non sia presente; 1 se il ledger
             * risulta corrotto; 2 nel caso la ricerca sia avvenuta con successo.
             * In quest'ultimo caso procedo a a ricevere prima la lunghezza della stringa "y" associata alla chiave,
             * infine, ricevo la stringa "y" */
            if(serverReturn == 0)
            {
                printError("Errore: chiave assente\n");
            }
            else if(serverReturn == 1)
            {
                printError("Errore: ledger corrotto\n");
            }
            else
            {
                int lengValue;
                if(receiveInt(socketDescriptor,&lengValue) < 0)
                {
                    perror("Errore");
                    exit(-1);
                }
                char* value = NULL;
                if(receiveString(socketDescriptor, &value, lengValue) < 0)
                {
                    perror("Errore");
                    exit(-1);
                }

                sprintf(printStr, "Il valore associato alla chiave %s è: %s\n", par1, value);
                printStr[strlen(printStr)] = '\0';
                printToMonitor(printStr);
            }
            break;
        case 3: // list
            /* Nel caso di list il server invierà il numero di coppie nella lista che verrà memorizzato in serverReturn.
             * Per inviare i valori contenuti nella invierà iterativamente:
             * lunghezza X -> valore X -> lunghezza y -> valore Y. 
             * Per ogni coppia di valori ricevuti si procederà alla stampa */
            printToMonitor("Gli elementi presenti nella lista sono:\n");
            if(serverReturn == 0)
            {
                printToMonitor("Lista vuota\n");
            }

            for(int i = 0; i < serverReturn; i++)
            {
                // ricevo la chiave
                if(receiveInt(socketDescriptor, &sizeX) < 0)
                {
                    perror("Errore");
                    exit(-1);
                }
                if(receiveString(socketDescriptor, &x, sizeX) < 0)
                {
                    perror("Errore");
                    exit(-1);
                }
                
                //ricevo il valore associato alla chiave
                if(receiveInt(socketDescriptor, &sizeY) < 0)
                {
                    perror("Errore");
                    exit(-1);
                }
                if(receiveString(socketDescriptor, &y, sizeY) < 0)
                {
                    perror("Errore");
                    exit(-1);
                }

                sprintf(printStr, "Coppia: %d\tChiave: %s\tValore: %s\n", i + 1, x, y);
                printStr[strlen(printStr)] = '\0';
                printToMonitor(printStr);

                free(x);
                free(y);
            }
            break;
        default: // corrupt o store
            /* Nel caso di store o corrupt il server invierà 1 nel caso l'operazione 
             * avvenga con successo, oppure 0 nel caso essa fallisca. 
             * (Es: X non presente nella lista per la corrupt o X già presente nella lista per la store) */
            if(serverReturn)
            {
                printToMonitor("Operazione avvenuta con successo\n");
            }
            else
            {
                printError("Operazione fallita\n");
            }
            break;
    }
    return 0;
}