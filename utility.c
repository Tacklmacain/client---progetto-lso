#include "utility.h"

/* ************************************************************************ */

/**
 * Converte la stringa in ingresso sostituendo i caratteri uppercase in lowercase
 * @param string la stringa da convertire (è anche il parametro di ritorno)
 */
void lowercase(char* string)
{
    int i = 0;
	while(string[i])
	{
	    // Se il singolo carattere della stringa è in questo range, allora è un carattere uppercase
		if((int)string[i] >= 65 && (int)string[i] <= 90)
		{
			string[i] += 32;
		}
		i++;
	}
}

// Controlla i requisiti minimi affinche una string POSSA essere un IP. Le verifiche definitive veranno fatte da inet_aton
bool strIsIP(const char* string)
{
    // Controllo che la lunghezza della stringa sia nel range giusto
    int lengSrt = strlen(string);
    if(lengSrt < MIN_SIZE_OF_IP || lengSrt > MAX_SIZE_OF_IP)
    {
        return false;
    }
    // Procedo a controllare i singoli caratteri che formano la stringa
    int i = 0, dots = 0;
	while(string[i])
	{
        // Se il singolo carattere della stringa non è compreso tra 48 e 57 allora non è un numero
		if(!((int)string[i] >= 48 && (int)string[i] <= 57))
		{
            // Se non è un punto allora non è un simbolo valido e ritorno false
            if((int)string[i] != 46)
			{
                return false;
            }
            // Altrimenti ho trovato un punto e incremento l'apposita variabile
            dots++;
		}
		i++;
	}
    
    // Nella stringa ci devono essere 3 punti
    if(dots != 3)
    {
        return false;
    }

    return true;
}

// Formatta l'ip nella versione più compatta eliminando gli 0 superflui
void reductoIp(char* ip)
{
    // Alloco una stringa della stessa dimensione di quella in input
    int lengthIp = strlen(ip);
    char* newIp = malloc(sizeof(char) * lengthIp);

    int j = 0;
    bool readZero = false;

    //Tramite il for analizzo ogni carattere della stringa in ingresso
    for(int i = 0; i < lengthIp; i++)
    {
        /* Se nella terna attuale sto leggendo un numero diverso da zero, un punto,
         * oppure ho trovato uno zero che è stato preceduto da un numero (garantito da readZero)
         * o è l'ultimo 0 della terna (se è immediatamente seguito da un '.' o da '\0'),
         * allora devo inserirlo nella nuova stringa */
        if(ip[i] != '0' || readZero == true || ip[i + 1] == '.' || ip[i + 1] == '\0')
        {
            if(ip[i] == '.')
            {
                // Se ho trovato un punto è terminata la terna attuale e reinizializzo readZero
                readZero = false;
            }
            else
            {
                /* Altrimenti, se mi trovo qui, ho un numero diverso da 0 oppure ho uno 0 valido 
                 * da leggere. In entrabi i casi si setta a true la possibilità di leggere altri 0 */
                readZero = true;
            }
            // Procedo a salvarmi il punto o un numero valido all'interno della nuova stringa
            newIp[j] = ip[i];
            j++;
        }
    }

    /* Se la grandezza del nuovo ip, contenuta in j, è diversa da quela precedente allora 
     * aggiungo il carattere terminatore e sovrascrivo il vecchio ip */ 
    if(j != lengthIp)
    {
        newIp[j] = '\0';
        strcpy(ip, newIp);
    }

    // Infine libero la memoria precedentemente allocata
    free(newIp);
}

/**
 * Verifica che la stringa in ingresso sia costituita da soli numeri
 * @param string la stringa da controllare
 * @return true se formata da soli numeri, false altrimenti
 */
bool strIsNumber(const char* string)
{
    int i = 0;
	while(string[i])
	{
        // Se il singolo carattere della stringa non è compreso tra 48 e 57 allora non è un numero
		if(!((int)string[i] >= 48 && (int)string[i] <= 57))
		{
			return false;
		}
		i++;
	}
    return true;
}

/**
 * Crea e connette il socket Client
 * @param address l'indirizzo in dotted notation del server
 * @param port la porta del server
 * @return socketDescritpor se la creazione e connessione ha avuto successo, -1 altrimenti
 */
int createClientSocket(char* address, int port) 
{
    // Creo il socket, lo imposto di rete (INET) e con connessione (STREAM)
    int socketDescriptor = socket(PF_INET, SOCK_STREAM, 0);
    if(socketDescriptor < 0) 
    {
        return -1;
    }

    // Per connettermi al server dichiaro un sockaddr del tipo del server
    struct sockaddr_in serverAddress;
    serverAddress.sin_family = AF_INET;
    serverAddress.sin_port = htons(port);

    // Converte da dotted notation a network e lo assegna, se fallisce ritorna 0, quindi l'indirizzo non è valido
    if(inet_aton(address, &serverAddress.sin_addr) == 0)
    {
        errno = EADDRNOTAVAIL;
        return -1;
    }

    // Se sono arrivato qui tutti i valori sono corretti
    printToMonitor("Parametri inseriti correttamente, procedo con la connessione al server...\n");

    /* Provo ad effettuare la connessione al server 10 volte attendendo un secondo tra i vari tentativi
     * Se uno di questi tentativi dovesse andare a buon fine, ritorno il socketDescriptor
     * Altrimenti ritorno -1 per segnalare l'errore */
    int numOfTry = 0;
    do {
        if (connect(socketDescriptor, (const struct sockaddr *) &serverAddress, sizeof(serverAddress)) == 0)
        {
            return socketDescriptor; // Connessione stabilita, ritorno il socketDescriptor
        }
        else 
        {
            // Se il risultato della connessione è < 0 allora è fallita
            numOfTry++;
            sleep(1);
        }
    } while(numOfTry < 10);

    // Se arrivo qui non sono riuscito a connettermi al server, ritorno -1
    return -1;
}

/**
 * Prende un intero dal socket
 * e della lettura dal socket che può essere minore della grandezza dell'intero
 * @param fd il file descriptor del socket su cui leggere
 * @param num il valore numerico che si intende leggere dal socket (è un parametro di ritorno)
 * @return 0 se ha successo, -1 altrimenti
 */
int receiveInt(int fd, int* num) 
{
    int32_t ret;
    char* data = (char*) &ret;
    int missingBytes = sizeof(ret); // Bytes mancanti
    int bytesRead;

    // Ciclo leggendo i bytes finché non leggo tutto l'intero
    do {
        bytesRead = read(fd, data, missingBytes);
        if(bytesRead < 0) 
        {
            return -1;
        }

        // Sposto il puntatore per continuare a leggere da quel byte
        data += bytesRead;

        // Decremento il numero di bytes mancanti da leggere
        missingBytes -= bytesRead;
    } while (missingBytes > 0);

    // Casto il risultato da network a valore host
    *num = ntohl(ret);
    return 0;
}

/**
 * Prende una stringa dal socket
 * @param fd il file descriptor del socket su cui leggere
 * @param str la stringa che si intende leggere dal socket (è un parametro di ritorno)
 * @param strLength la lunghezza della stringa
 * @return 0 se ha successo, -1 altrimenti
 */
int receiveString(int fd, char** str, int strLength) 
{
    char* uncompleteStr = malloc(sizeof(char) * strLength);
    *str = calloc(strLength, sizeof(char));    // Alloco la stringa
    int bytesToReceive = strLength;
    int bytesReceived;

    // Ciclo finché non ricevo tutti i bytes necessari
    do {
        bytesReceived = read(fd, uncompleteStr, bytesToReceive);
        if(bytesReceived < 0) 
        {
            free(uncompleteStr);
            return -1;
        }

        // Copio quello che ho letto finora nella stringa completeStr
        uncompleteStr[bytesReceived - 1] = '\0';
        strcat(*str, uncompleteStr);

        // Decremento il numero di byte che mi mancano da ricevere
        bytesToReceive -= bytesReceived;
    } while(bytesToReceive > 0);

    free(uncompleteStr);
    return 0;
}

/**
 * Scrive un intero sul socket tenendo conto della differenza di rappresentazione di un int
 * @param fd il file descriptor del socket su cui scrivere
 * @param num il valore numerico che si intende scrivere sul socket
 * @return 0 se ha successo, -1 altrimenti
 */
int sendInt(int fd, int num) 
{
    int32_t conv = htonl(num);
    char* data = (char*) &conv;
    int missingBytes = sizeof(conv);
    int bytesWritten;

    // Ciclo scrivendo i bytes finché non scrivo tutto l'intero
    do {
        bytesWritten = write(fd, data, missingBytes);
        if (bytesWritten < 0) 
        {
            return -1;
        }

        // Sposto il puntatore per continuare a scrivere da quel byte
        data += bytesWritten;

        // Decremento il numero di bytes mancanti da scrivere
        missingBytes -= bytesWritten;
    } while (missingBytes > 0);

    return 0;
}

/**
 * Scrive una stringa sul socket, necessaria in quanto una write può scrivere meno byte di quanto dovrebbe
 * @param fd il file descriptor del socket su cui scrivere
 * @param str la stringa che si intende scrivere sul socket
 * @param strLength la grandezza della stringa
 * @return 0 se ha successo, -1 altrimenti
 */
int sendString(int fd, char* str, int strLength)
{
    // Bytes mancanti, si potrebbe usare strLenght ma una nuova variabile ne aumenta la leggibilità
    int missingBytes = strLength;
    int bytesWritten;

    // Ciclo scrivendo i bytes finché non scrivo tutto la stringa
    do {
        bytesWritten = write(fd, str, missingBytes);
        if (bytesWritten < 0)
        {
            return -1;
        }

        // Sposto il puntatore per continuare a scrivere da quel byte
        str += bytesWritten;

        // Decremento il numero di bytes mancanti da scrivere
        missingBytes -= bytesWritten;
    } while (missingBytes > 0);

    return 0;
}

/**
 * Scrive una stringa sullo standard output tramite funzioni non bufferizzate
 * @param message la stringa da stampare
 */
void printToMonitor(char* message) 
{
    sendString(STDOUT_FILENO, message, strlen(message));
}

/**
 * Scrive una stringa sullo standard error tramite funzioni non bufferizzate
 * @param message la stringa da stampare
 */
void printError(char* message)
{
    sendString(STDERR_FILENO, message, strlen(message));
}